/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @添加到组 Wpa
 * @{
 *
 * @brief 定义上层WLAN服务的API接口.
 *
 * 您可以使用API启用或禁用WLAN热点、扫描热点、连接到WLAN热点,管理WLAN芯片、网络设备和电源,并申请、释放和移动网络数据缓冲区.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IWpaInterface.idl
 *
 * @brief 提供API以启用或禁用WLAN热点、扫描热点、连接到WLAN热点或断开与WLAN热点的连接,设置国家代码,并管理网络设备.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief 定义Wpa模块接口的包路径.
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.wlan.wpa.v1_0;

import ohos.hdi.wlan.wpa.v1_0.WpaTypes;
import ohos.hdi.wlan.wpa.v1_0.IWpaCallback;

/**
 * @brief 定义上层WLAN服务的接口.
 *
 * @since 4.1
 * @version 1.0
 */

 interface IWpaInterface {
    /**
     * @brief 在HAL和wpa请求端之间创建一个通道,并获取驱动程序网络接口卡(NIC)信息,此函数必须在创建IWiFi实例之后调用.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    Start();

    /**
     * @brief 销毁HAL和wpa请求者之间的通道。此函数必须在IWiFi实例销毁之前调用.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    Stop();

    /**
     * @brief 在wpa请求方中添加接口.
     *
     * @param ifName 表示需要添加的接口（如：wlan0或wlan2）.
     * @param confName 表示配置文件（例如：/data/service/el1/public/wifi/wpa_ppliet/wpa_pplicant.conf).
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    AddWpaIface([in] String ifName, [in] String confName);
	
    /**
     * @brief 删除wpa请求方中的接口.
     *
     * @param ifName 表示需要删除的接口（例如：wlan0或wlan2）.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    RemoveWpaIface([in] String ifName);

    /**
     * @brief 在wpa请求方中扫描.
     *
     * @param ifName 表示网卡(NIC)名称.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    Scan([in] String ifName);

    /**
     * @brief wpa请求方中的扫描结果.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param resultBuf 表示已获得扫描结果. 
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    ScanResult([in] String ifName, [out] unsigned char[] resultBuf);

    /**
     * @brief 在wpa请求方中添加网络.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param networkId 表示添加的网络ID.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    AddNetwork([in] String ifName, [out] int networkId);

    /**
     * @brief 删除wpa请求方中的网络.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param networkId 表示要删除的网络ID.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    RemoveNetwork([in] String ifName, [in] int networkId);

    /**
     * @brief 禁用wpa请求方中的网络.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param networkId 表示要禁用的网络ID.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    DisableNetwork([in] String ifName, [in] int networkId);

    /**
     * @brief 在wpa请求方中设置网络.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param networkId 表示要设置的网络ID
     * @param name 表示要设置的名称
     * @param value 表示要设置的值
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    SetNetwork([in] String ifName, [in] int networkId, [in] String name, [in] String value);

    /**
     * @brief wpa请求方中的网络列表.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param mode 表示已获取wifi网络信息. 
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    ListNetworks([in] String ifName, [out] struct HdiWifiWpaNetworkInfo[] networkInfo);

    /**
     * @brief 在wpa请求方中选择网络.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param networkId 表示网络ID选择.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    SelectNetwork([in] String ifName, [in] int networkId);
     
    /**
     * @brief 在wpa请求方中启用网络.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param networkId 表示待启动的网络ID.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    EnableNetwork([in] String ifName, [in] int networkId);

    /**
     * @brief wpa请求方重新连接网络.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    Reconnect([in] String ifName);

    /**
     * @brief wpa请求方中断开连接.
     *
     * @param ifName 表示网卡(NIC)名称.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    Disconnect([in] String ifName);
      
    /**
     * @brief 保存wpa请求方配置.
     *
     * @param ifName 表示网卡(NIC)名称.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    SaveConfig([in] String ifName);

    /**
     * @brief 在wpa请求方中设置PowerSave.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param enable 表示是否设置powerSave .
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    SetPowerSave([in] String ifName, [in] int enable);  

    /**
     * @brief wpa请求方中的自动连接.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param enable 表示是否自动连接 .
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    AutoConnect([in] String ifName, [in] int enable);

    /**
     * @brief 获取wpa请求方Wifi状态.
     *
     * @param ifName 表示网卡(NIC)名称. 
     * @param mode 表示已获得wifi状态. 
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    WifiStatus([in] String ifName, [out] struct HdiWpaCmdStatus wifiStatus);

    /**
     * @brief 设置wpa请求方WpsPbcMode.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param wpsParam 表示已获得wifi状态. 
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    WpsPbcMode([in] String ifName, [in] struct HdiWifiWpsParam wpsParam);

    /**
     * @brief 设置wpa请求方Wifi状态.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param wpsParam 表示已获得wifi状态. 
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    WpsPinMode([in] String ifName, [in] struct HdiWifiWpsParam wpsParam, [out] int pinCode);

    /**
     * @brief 取消wpa请求程序中的Wps.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    WpsCancel([in] String ifName);

    /**
     * @brief 获取wpa请求方国家码.
     *
     * @param ifName 表示网卡(NIC)名称.     
     * @param countrycode 表示获得的国家码.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    GetCountryCode([in] String ifName, [out] String countrycode);    

    /**
     * @brief 获取wpa请求方网络.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param networkId 表示参数的网络ID
     * @param param 表示参数
     * @param value 表示获得的值.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    GetNetwork([in] String ifName, [in] int networkId, [in] String param, [out] String value);

    /**
     * @brief 清除wpa请求方的黑名单.
     *
     * @param ifName 表示网卡(NIC)名称.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    BlocklistClear([in] String ifName);

    /**
     * @brief 设置wpa请求方的SuspendMode.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param mode 表示设置挂起 .
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    SetSuspendMode([in] String ifName, [in] int mode);

    /**
     * @brief 注册回调以侦听异步事件.
     *
     * @param cbFunc 表示要注册的回调.
     * @param ifName 表示网卡(NIC)名称.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    RegisterEventCallback([in] IWpaCallback cbFunc, [in] String ifName);

    /**
     * @brief 销毁注回调.
     *
     * @param cbFunc 表示要注销的回调.
     * @param ifName 表示网卡(NIC)名称.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    UnregisterEventCallback([in] IWpaCallback cbFunc, [in] String ifName);

    /**
     * @brief 获取wpa请求方中的连接能力.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param connectionCap 表示获取到的connectionCap. 
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    GetConnectionCapabilities([in] String ifName, [out] struct ConnectionCapabilities connectionCap);

    /**
     * @brief 获取是否为此网络发送探测请求（隐藏）.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param enabled 如果设置,则为true,否则为false.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    GetScanSsid([in] String ifName, [out] int enable);

    /**
     * @brief Get passphrase in wpa supplicant.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param psk 表示设置psk的值.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    GetPskPassphrase([in] String ifName, [out] String psk);

    /**
     * @brief 获取wpa请求方的原始psk.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param psk 表示psk值集.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
     GetPsk([in] String ifName, [out] unsigned char[] psk);

    /**
     * @brief 获取wpa请求方WEP密钥.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param keyIdx 要提取的wep密钥的索引
     * @param wepKey wep键值集.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
     GetWepKey([in] String ifName, [in] int keyIdx, [out] unsigned char[] wepKey);

    /**
     * @brief 获取wpa请求方的默认Tx密钥索引.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param keyIdx 表示keyIdx的值集.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    GetWepTxKeyIdx([in] String ifName, [out] int keyIdx);

    /**
     * @brief 获取是否为此网络启用RequirePmf.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param enabled 如果设置,则为true,否则为false.
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
     GetRequirePmf([in] String ifName, [out] int enable);

    /**
     * @brief 设置wpa请求方国家码.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param countrycode 表示要设置的国家码
     *
     * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */    
    SetCountryCode([in] String ifName,[in] String countrycode);

    /**
    * 设置要用于P2P SSID的后缀.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param name 表示要挂到SSID的字符串.
    * 
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    */
    P2pSetSsidPostfixName([in] String ifName, [in] String name);

    /**
    * @brief 为p2p设置Wps设备类型.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param type 表示wpa设备类型.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pSetWpsDeviceType([in] String ifName, [in] String type);

    /**
    * @brief set Wps config methods for p2p.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param methods 表示参数的Wps配置方法.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pSetWpsConfigMethods([in] String ifName, [in] String methods);

    /**
    * @brief P2P组的最大空闲时间(以秒为单位).
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param time 表示参数的最大空闲时间.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pSetGroupMaxIdle([in] String ifName, [in] int time);

    /**
    * @brief 启用/禁用对等网络的Wifi显示.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param enable 1启用，0禁用.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pSetWfdEnable([in] String ifName, [in] int enable);

    /**
    * @brief 为p2p设置永久重新连接.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param status 表示参数的“永久重新连接”状态.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pSetPersistentReconnect([in] String ifName, [in] int status);

    /**
    * @brief 为p2p设置Wps辅助设备类型.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param type 表示参数的wpa辅助设备类型.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pSetWpsSecondaryDeviceType([in] String ifName, [in] String type);
    
    /**
    * @brief 为p2p设置Wps-pbc.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param address 表示参数的AP的BSSID.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */  
    P2pSetupWpsPbc([in] String ifName, [in] String address);

    /**
    * @brief 为p2p设置Wps引脚.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param address 表示参数的AP的BSSID.
    * @param pin 要使用的8位引脚.
    * @param result 表示操作的状态.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */ 
    P2pSetupWpsPin([in] String ifName, [in] String address, [in] String pin, [out] String result);
    
    /**
    * @brief 打开/关闭接口的省电模式.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param enable 表示是否要打开/关闭节能.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    */  
    P2pSetPowerSave([in] String ifName, [in] int enable);

    /**
    * @brief Set Device Name for p2p.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param name 表示参数的设备名称.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */  
    P2pSetDeviceName([in] String ifName, [in] String name);

    /**
    * @brief 为p2p设置Wifi显示设备配置.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param config 表示参数的设备配置.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pSetWfdDeviceConfig([in] String ifName, [in] String config);

    /**
    * @brief 为p2p设置随机MAC.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param networkId 表示网络ID启用.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pSetRandomMac([in] String ifName, [in] int networkId);

    /**
    * @brief 启动p2p查找.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param timeout 表示执行查找所用的最长时间.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pStartFind([in] String ifName, [in] int timeout);

    /**
    * @brief 为p2p配置扩展监听定时.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param enable 表示是否启用.
    * @param period 表示周期（以毫秒为单位）.
    * @param enable 表示间隔（以毫秒为单位）.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pSetExtListen([in] String ifName, [in] int enable, [in] int period, [in] int interval);

    /**
    * @brief 设置P2P监听通道.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param channel 表示Wifi信道.
    * @param regClass 表示此BSSID指示的AP的信道集.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pSetListenChannel([in] String ifName, [in] int channel, [in] int regClass);

    /**
    * @brief 向指定的对等方发送P2P供应发现请求.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param peerBssid 表示要发送发现的设备的MAC地址.
    * @param mode 指示参数的设置模式.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pProvisionDiscovery([in] String ifName, [in] String peerBssid, [in] int mode);

    /**
    * @brief 手动设置P2P组所有者.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param isPersistent 表示用于请求组成持久组.
    * @param networkId 表示网络ID启用.
    * @param freq 表示p2p组的频率.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pAddGroup([in] String ifName, [in] int isPersistent, [in] int networkId, [in] int freq);

    /**
    * @brief 为p2p添加服务.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param info 表示P2p服务信息.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pAddService([in] String ifName, [in] struct HdiP2pServiceInfo info);

    /**
    * @brief 删除p2p服务.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param info 表示P2p服务信息.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pRemoveService([in] String ifName, [in] struct HdiP2pServiceInfo info);
    
    /**
    * @brief 停止正在进行的P2P服务发现.
    *
    * @param ifName 表示网卡(NIC)名称.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pStopFind([in] String ifName);

    /**
    * @brief 刷新P2P设备表和状态.
    *
    * @param ifName 表示网卡(NIC)名称.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pFlush([in] String ifName);

    /**
    * @brief 此命令可用于从设备中清除所有服务.
    *
    * @param ifName 表示网卡(NIC)名称.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pFlushService([in] String ifName);

    /**
    * @brief 删除p2p网络.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param networkId 表示网络ID启用.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pRemoveNetwork([in] String ifName, [in] int networkId);

    /**
    * @brief 为p2p设置组配置.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param networkId 表示网络ID启用.
    * @param name 表示参数的组配置名称.
    * @param value 表示参数的组配置值.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pSetGroupConfig([in] String ifName, [in] int networkId, [in] String name, [in] String value);

    /**
    * @brief 为p2p设置组配置.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param peerAddress 要邀请的设备的MAC地址.
    * @param goBssid 组所有者设备的MAC地址.
    * @param ifName 表示网卡(NIC)名称.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pInvite([in] String ifName, [in] String peerBssid, [in] String goBssid);

    /**
    * @brief 重新为p2p设置组配置.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param networkId 表示网络ID启用.
    * @param bssid 要重新配置的设备的MAC地址.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pReinvoke([in] String ifName, [in] int networkId,[in] String bssid);

    /**
    * @brief 获取设备地址.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param deviceAddress 表示设备地址信息.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pGetDeviceAddress([in] String ifName, [out] String deviceAddress);

    /**
    * @brief 安排P2P服务发现请求.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param reqService 表示对等设备的设备mac地址.
    * @param replyDisc 表示服务发现序列.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pReqServiceDiscovery([in] String ifName, [in] struct HdiP2pReqService reqService,[out] String replyDisc);

    /**
    * @brief 取消以前的服务发现请求.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param id 取消请求的ID.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pCancelServiceDiscovery([in] String ifName, [in] String id);

    /**
    * @brief p2p服务器发现的resp
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param info 表示服务器发现的响应信息.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pRespServerDiscovery([in] String ifName, [in] struct HdiP2pServDiscReqInfo info);

    /**
    * @brief 使用已发现的P2P队，开始P2P建连.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param info 表示要连接的设备的所有消息.
    * @param replyPin 生成Pin,如果|provisionMethod|使用生成的|Pin*|方法之一.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pConnect([in] String ifName, [in] struct HdiP2pConnectInfo info, [out] String replyPin);

    /**
    * @brief 使用已发现的P2P队，开始P2P建连.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param info 表示要连接的设备的所有消息.
    * @param replyPin 生成Pin,如果|provisionMethod|使用生成的|Pin*|方法之一.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */
    P2pHid2dConnect([in] String ifName, [in] struct HdiHid2dConnectInfo info);

    /**
    * @brief 为p2p设置服务发现模式
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param mode 指示参数的服务发现模式.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */ 
    P2pSetServDiscExternal([in] String ifName, [in] int mode);

    /**
    * @brief 为p2p删除组
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param groupName 表示p2p的组名.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */ 
    P2pRemoveGroup([in] String ifName, [in] String groupName);

    /**
    * @brief 取消p2p连接
    *
    * @param ifName 表示网卡(NIC)名称.
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */ 
    P2pCancelConnect([in] String ifName);

    /**
    * @brief 获取p2p的组配置
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param networkId 表示网络ID启用.
    * @param param 表示组配置名称.
    * @param value 表示组配置值.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */ 
    P2pGetGroupConfig([in] String ifName, [in] int networkId, [in] String param, [out] String value);

    /**
    * @brief 为p2p添加网络
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param networkId 表示网络ID启用.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */ 
    P2pAddNetwork([in] String ifName, [out] int networkId);

    /**
    * @brief 获取设备所属组的功能.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param bssid 表示对等方的MAC地址.
    * @param info 表示用于保存设备信息的结构体.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */    
    P2pGetPeer([in] String ifName, [in] String bssid, [out] struct HdiP2pDeviceInfo info);

    /**
    * @brief 获取设备所属组的功能.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param bssid 表示对等方的MAC地址.
    * @param cap 表示功能掩码|P2pGroupCapabilityMmask|值的组合.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */ 
    P2pGetGroupCapability([in] String ifName, [in] String bssid, [out] int cap);

    /**
    * @brief 列出所有网络信息.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @param infoList 表示用于保存网络信息的结构体.
    *
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */ 
    P2pListNetworks([in] String ifName, [out] struct HdiP2pNetworkList infoList);
    
    /**
    * @brief 为p2p保存配置.
    *
    * @param ifName 表示网卡(NIC)名称.
    * @return 返回值 如果操作成功，则返回0.
    * @return 返回值 如果操作失败，则为负值.
    *
    * @since 4.1
    * @version 1.0
    */ 
    P2pSaveConfig([in] String ifName);

    /**
     * @brief 重新关联wpa请求者.
     *
     * @param ifName 表示网卡(NIC)名称.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
    Reassociate([in] String ifName);

    /**
     * @brief Wpa请求者STA CMD.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param cmd 表示WifiHal对Sta的命令
     * 示例：如果CMD为“SET external_sim 1”，则最终结果是“externalsim=1”.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.1
     * @version 1.0
     */
     StaShellCmd([in] String ifName, [in] String cmd);
 }