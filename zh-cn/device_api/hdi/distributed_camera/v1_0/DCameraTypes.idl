/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Distributed Camera
 * @{
 *
 * @brief Distributed Camera模块接口定义。
 *
 * Distributed Camera模块包括对分布式相机设备的操作、流的操作和各种回调等，这部分接口与Camera一致。
 * 通过IDCameraProvider与Source SA通信交互，实现分布式功能。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file DCameraTypes.idl
 *
 * @brief Distributed Camera模块HDI接口使用的数据类型。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Distributed Camera设备接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.distributed_camera.v1_0;

/**
 * @brief 分布式相机metadata更新类型。
 *
 * @since 3.2
 * @version 1.0
 */
enum DCSettingsType {
    /**
     * 设置整包数据。
     */
    UPDATE_METADATA = 0,
    /**
     * 使能数据配置。
     */
    ENABLE_METADATA = 1,
    /**
     * 去使能数据配置。
     */
    DISABLE_METADATA = 2,
    /**
     * 分布式相机返回结果。
     */
    METADATA_RESULT = 3,
    /**
     * 闪光灯设置。
     */
    SET_FLASH_LIGHT = 4,
    /**
     * fps范围设置。
     */
    FPS_RANGE = 5,
    /**
     * 帧数设置。
     */
    UPDATE_FRAME_METADATA = 6,
};

/**
 * @brief HDI接口的返回值。
 *
 * @since 3.2
 * @version 1.0
 */
enum DCamRetCode {
    /**
     * 调用成功。
     */
    SUCCESS = 0,
    /**
     * 设备当前忙。
     */
    CAMERA_BUSY = 1,
    /**
     * 参数错误。
     */
    INVALID_ARGUMENT = 2,
    /**
     * 不支持当前调用方法。
     */
    METHOD_NOT_SUPPORTED = 3,
    /**
     * 设备已经下线。
     */
    CAMERA_OFFLINE = 4,
    /**
     * 使能的分布式相机设备超过限制。
     */
    EXCEED_MAX_NUMBER = 5,
    /**
     * 设备没有初始化。
     */
    DEVICE_NOT_INIT = 6,
    /**
     * 调用失败。
     */
    FAILED = 7,
};

/**
 * @brief 流数据的编码类型。
 *
 * @since 3.2
 * @version 1.0
 */
enum DCEncodeType {
    /**
     * 未设置编码类型。
     */
    ENCODE_TYPE_NULL = 0,
    /**
     * 编码类型为H264。
     */
    ENCODE_TYPE_H264 = 1,
    /**
     * 编码类型为H265。
     */
    ENCODE_TYPE_H265 = 2,
    /**
     * 编码类型为JPEG。
     */
    ENCODE_TYPE_JPEG = 3,
    /**
     * 编码类型为mpeg4-es。
     */
    ENCODE_TYPE_MPEG4_ES = 4,
};

/**
 * @brief 内部流的类型。
 *
 * @since 3.2
 * @version 1.0
 */
enum DCStreamType {
    /**
     * 连续流。例如：预览流，录像流。
     */
    CONTINUOUS_FRAME = 0,
    /**
     * 单个捕获流。例如：拍照流。
     */
    SNAPSHOT_FRAME = 1,
};

/**
 * @brief 分布式硬件设备基础信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct DHBase {
    /**
     * 设备Id。
    */
    String deviceId_;
    /**
     * 分布式硬件Id。
     */
    String dhId_;
};

/**
 * @brief 分布式相机控制设置。
 *
 * @since 3.2
 * @version 1.0
 */
struct DCameraSettings {
    /**
     * 设置类型，具体类型查看{@link DCSettingsType}。
     */
    enum DCSettingsType type_;
    /**
     * Settings值的序列化值。
     */
    String value_;
};

/**
 * @brief 分布式相机内部流信息，用于创建流时传入相关的配置参数。
 *
 * @since 3.2
 * @version 1.0
 */
struct DCStreamInfo {
    /**
     * 流的ID，用于在设备内唯一标识一条流。
     */
    int streamId_;
    /**
     * 图像宽度，具体宽度查看{@link StreamInfo}。
     */
    int width_;
    /**
     * 图像高度，具体高度查看{@link StreamInfo}。
     */
    int height_;
    /**
     * 图像步幅，具体步幅查看{@link StreamInfo}。
     */
    int stride_;
    /**
     * 图像格式，具体格式查看{@link StreamInfo}。
     */
    int format_;
    /**
     * 图像颜色空间，具体颜色空间查看{@link StreamInfo}。
     */
    int dataspace_;
    /**
     * 编码类型，具体类型查看{@link DCEncodeType}。
     */
    enum DCEncodeType encodeType_;
    /**
     * 流类型，具体类型查看{@link DCStreamType}。
     */
    enum DCStreamType type_;
};

/**
 * @brief 分布式相机内部捕获请求的信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct DCCaptureInfo {
    /**
     * 捕获的流ID集合。
    */
    int[] streamIds_;
    /**
     * 捕获的图像宽度。
     */
    int width_;
    /**
     * 捕获的图像高度。
     */
    int height_;
    /**
     * 捕获的图像步幅。
     */
    int stride_;
    /**
     * 捕获的图像格式。
     */
    int format_;
    /**
     * 捕获的图像颜色空间。
     */
    int dataspace_;
    /**
     * 是否触发接收捕获。
     */
    boolean isCapture_;
    /**
     * 捕获的编码类型，具体类型查看{@link DCEncodeType}。
     */
    enum DCEncodeType encodeType_;
    /**
     * 捕获的流类型，具体类型查看{@link DCStreamType}。
     */
    enum DCStreamType type_;
    /**
     * 捕获的配置信息，具体定义查看{@link DCameraSettings}。
     */
    struct DCameraSettings[] captureSettings_;
};

/**
 * @brief 分布式相机进程间传递数据的共享内存结构体。
 *
 * @since 3.2
 * @version 1.0
 */
struct DCameraBuffer {
    /**
     * 缓冲区索引。
     */
    int index_;
    /**
     * 缓冲区大小。
     */
    unsigned int size_;
    /**
     * Native缓冲区，具体定义查看{@link NativeBuffer}。
     */
    NativeBuffer bufferHandle_;
};

/**
 * @brief 分布式相机的通知事件。
 *
 * @since 3.2
 * @version 1.0
 */
struct DCameraHDFEvent {
    /**
     * 事件类型。具体类型查看{@link DCameraEventType}。
     */
    int type_;
    /**
     * 事件结果。具体结果查看{@link DCameraEventResult}。
     */
    int result_;
    /**
     * 扩展内容（可选）。
     */
    String content_;
};
