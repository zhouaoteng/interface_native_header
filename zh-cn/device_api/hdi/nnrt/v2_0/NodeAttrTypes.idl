/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup NNRt
 * @{
 *
 * @brief Neural Network Runtime（NNRt, 神经网络运行时）是面向AI领域的跨芯片推理计算运行时，作为中间桥梁连通上层AI推理框架和底层加速芯片，实现AI模型的跨芯片推理计算。提供统一AI芯片驱动接口，实现AI芯片驱动接入OpenHarmony。
 *
 * @since 3.2
 * @version 2.0
 */

/**
 * @file NodeAttrTypes.idl
 *
 * @brief 该文件定义AI模型算子的参数和功能。
 * 
 * 该文文件中所有的结构体仅声明了算子的属性，并不包含执行算子函数的接口，具体介绍如下：
 * - 1. 该文件中每一个算子都与{@link NodeType}的枚举值一一对应，执行模型推理时，{@link NodeType}会在{@link Node}的nodeType中存储；
 * - 2. 每一个算子都至少有一个“输入”与“输出”，“输入”即为该算子接收的张量，“输出”为经过算子运算之后得到的“张量”；“输入”、“算子”和“输出”之间的关系需要通过{@link Node}结构体的inputIndex和outIndex来确认。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief NNRt模块的包路径。
 *
 * @since 3.2
 * @version 2.0
 */
package ohos.hdi.nnrt.v2_0;

import ohos.hdi.nnrt.v2_0.NnrtTypes;

/**
 * @brief 激活类型的算子，所有的激活函数都属于该算子，具体的激活函数类型一句参数来确定。
 * 
 * 该算子对应的{@link NodeType}为NODE_TYPE_ACTIVATION。
 *
 * 输入：
 *
 * * x，n维张量。
 *
 * 输出：
 *
 * * 输出x经过激活函数之后的张量。
 *
 * @since 3.2
 * @version 1.0
 */
struct Activation
{
    /** 激活函数类型。 */
    enum ActivationType activationType;
    /** 尺寸因子，用于LeakyReLU和ELU激活函数。 */
    float alpha;
    /** 最小值，用于HardTanh激活函数。 */
    float minVal;
    /** 最大值，用于HardTanh激活函数。 */
    float maxVal;
    /** 是否使用近似算法，用于GRLU激活函数。 */
    boolean approximate;
};

/**
 * @brief 输入Tensor逐元素相加， 输出x和y的和，数据形状与输入broadcast之后一样，数据类型与较高精度的输入精度一致。
 * 
 * 该算子对应的{@link NodeType}为NODE_TYPE_ADD_FUSION。
 *
 * 输入：
 *
 * * x，第一个输入张量。
 * * y，第二个输入张量，数据类型和第一个张量保持一致。
 *
 * * 输出：
 *
 * * output，x和y逐元素相加， 输出x和y的和，数据形状与输入broadcast之后一样，数据类型与较高精度的输入精度一致。
 *      如果配置了activationType则会在输出之前调用指定的激活函数。
 *
 * @since 3.2
 * @version 1.0
 */
struct AddFusion
{
    /** 激活函数类型。详情请参考：{@link ActivationType} */
    enum ActivationType activationType;
};

/**
 * @brief 返回跨轴的tensor前K个索引或者是数值。
 * 
 * 该算子对应的{@link NodeType}为NODE_TYPE_ARGMAX_FUSION。
 *
 *
 * 输入：
 *
 * * x，n维tensor，输入张量（N,*）,其中*意味着数量任意的附加维度。
 *
 * 输出：
 *
 * * output，轴上输入张量最大值的前K个索引或者是数值。
 *
 * @since 3.2
 * @version 1.0
 */
struct ArgMaxFusion
{
    /** 指定求最大值索引的维度。 */
    long axis;
    /** 轴上前K个最大值。 */
    long topK;
    /** 是否保持输出维度和输入的维度是否相同。 */
    boolean keepDims;
    /** 若为false则输出索引，为true则输出数值，默认为false。 */
    boolean outMaxValue;
};

/**
 * @brief 在输入tensor上应用 2D 平均池化。支持int8量化输入。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_AVGPOOL_FUSION。
 *
 * 参数限制：当padMode==PAD_MODE_PAD时，padList的数值必须大于等于0。其他情况下padding的数值必须为0。
 *
 * 输入：
 * 
 * * x，n维张量。
 *
 * 输出：
 *
 * * output， 输出平均池化后的张量。
 *
 * @since 3.2
 * @version 1.0
 */
struct AvgPoolFusion
{
    /**
     * 用来取平均值的kernel大小，是一个长度为2的int数组[kernel_height，kernel_weight]，
     * 第一个数表示kernel高度，第二个数表示kernel宽度。
     */
    long[] kernelSize;
    /**
     * kernel移动的距离，是一个长度为2的int数组[stride_height，stride_weight]，
     * 第一个数表示高度上的移动步幅，第二个数表示宽度上的移动步幅。
     */
    long[] strides;
    /**  x周围的填充，是一个长度为4的int数组[top，bottom，left，right]，并且以最近邻的值填充。 */
    long[] pad;
    /** 填充模式 */
    enum PadMode padMode;
    /** 取整数的算法 */
    enum RoundMode roundMode;
    /** 运算时的数据排列排列，详情请参考：{@link Format} */
    enum Format format;
    /** 是否是全局池化 */
    boolean global;
    /** 激活函数，详情请参考：{@link ActivationType} */
    enum ActivationType activationType;
};

/**
 * @brief 将一个4维tensor的batch维度按block_shape切分成小块，并将这些小块拼接到空间维度。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_BATCH_TO_SPACE_ND。
 *
 * 输入：
 *
 * * x，n维tensor。
 *
 * 输出：
 *
 * * 输出张量，假设x的形状为(n,h,w,c)，output的形状为（n',h',w',c'）：
 * \f$ n' = n / (block_shape[0] * block_shape[1])\f$<br>
 * \f$ h' = h * block_shape[0] - crops[0][0] - crops[0][1] \f$<br>
 * \f$ w' = w * block_shape[1] - crops[1][0] - crops[1][1] \f$<br>
 * \f$ c'= c \f$
 *
 * @since 3.2
 * @version 1.0
 */
struct BatchToSpaceND
{
    /** 一个长度为2的数组[height_block，weight_block]，指定切分到空间维度上的block大小。*/
    long[] blockShape;
    /**
     * 一个shape为(2，2)的2维数组[[crop0_start，crop0_end]，[crop1_start，crop1_end]]，
     * 表示在output的空间维度上截掉部分元素。
     */
    long[][] crops;
};

/**
 * @brief 对给出的输入张量上的各个维度方向上的数据进行偏置。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_BIAS_ADD。
 *
 * 输入：
 *
 * * x，n维tensor。
 * * bias，偏置值tensor。
 *
 * 输出：
 *
 * * 输出张量，根据输入中每个维度方向偏置之后的结果。
 *
 * @since 3.2
 * @version 1.0
 */
struct BiasAdd
{
};

/**
 * @brief 根据输出张量的类型对张量数据类型进行转换。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_CAST。
 *
 * 输入：
 *
 * * x，n维tensor
 * * type，输入转换目的的数据类型。
 *
 * 输出：
 *
 * * output，按照输出tensor的数据类型进行类型转换。
 *
 * @since 3.2
 * @version 1.0
 */
struct Cast
{
};

/**
 * @brief 在指定轴上连接张量，将输入张量按给定的轴连接起来。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_CONCAT。
 *
 * 输入：
 *
 * * 多个维度相同的tensor。
 *
 * 输出：
 *
 * * output，多个张量按axis轴连接的结果。
 *
 * @since 3.2
 * @version 1.0
 */
struct Concat
{
    /** 拼接的轴，axis必须小于输入张量的维度数量。 */
    long axis;
};

/**
 * @brief 对将4维的tensor执行带有偏置的二维卷积运算。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_CONV2D_FUSION。
 *
 * 参数限制：当padMode==PAD_MODE_PAD时，padList的数值必须大于等于0。其他情况下padding的数值必须为0。
 *
 * 输入：
 *
 * * x，4维tensor，并按照NHWC进行排列。
 * * weight，卷积的权重，要求weight排布为[outChannel，kernelHeight，kernelWidth，inChannel/group]，
 *      inChannel必须要能整除group。
 * * bias，卷积的偏置，是长度为[outChannel]的数组。在量化场景下，bias 参数不需要量化参数，其量化
 *      版本要求输入 int32 类型数据，实际量化参数由 x 和 weight 共同决定。
 *
 * 输出：
 *
 * * output，卷积的输出。
 *
 * @since 3.2
 * @version 1.0
 */
struct Conv2DFusion
{
    /** 卷积核大小。 */
    long[] kernelSize;
    /** 卷积核在height和weight上的步幅，是一个长度为2的int数组[strideHeight，strideWidth]。 */
    long[] stride;
    /**
     * 表示扩张卷积在height和weight上的扩张率，是一个长度为2的int数组[dilationHeight，dilationWidth]，
     * 值必须大于或等于1，并且不能超过x的height和width。
     */
    long[] dilation;
    /** 填充类型，详情请参考：{@link PadMode}。 */
    enum PadMode padMode;
    /**输入x周围的填充，是一个长度为4的int数组[top，bottom，left，right]。 */
    long[] padList;
    /**
     * group，将输入x按inChannel分组，int类型。
     * group等于1，这是常规卷积。
     * group等于inChannel，这是depthwiseConv2d，此时group==in_channel==out_channel。
     * group大于1且小于inChannel，这是分组卷积，此时out_channel==group。
     */
    long group;
    /** 输入通道数量。 */
    long inChannel;
    /** 输出通道数量。 */
    long outChannel;
    /** 激活函数类型，详情请参考：{@link ActivationType}。*/
    enum ActivationType activationType;
};

/**
 * @brief 对一个4维的tensor执行带有偏置的二维反卷积。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_CONV2D_TRANSPOSE_FUSION。
 *
 * 参数限制：当padMode==PAD_MODE_PAD时，padList的数值必须大于等于0。其他情况下padding的数值必须为0。
 *
 * 输入：
 *
 * * x，4维tensor，并按照NHWC进行排列。
 * * weight，卷积的权重，要求weight排布为[outChannel，kernelHeight，kernelWidth，inChannel/group]，
 *      inChannel必须要能整除group。
 * * bias，卷积的偏置，是长度为[outChannel]的数组。在量化场景下，bias 参数不需要量化参数，其量化
 *      版本要求输入 int32 类型数据，实际量化参数由 x 和 weight 共同决定。
 *
 * 输出：
 *
 * *  output，n维tensor。
 *
 * @since 3.2
 * @version 1.0
 */
struct Conv2dTransposeFusion
{
    /** 卷积核大小。 */
    long[] kernelSize;
    /** 卷积核在height和weight上的步幅，是一个长度为2的int数组[strideHeight，strideWidth]。 */
    long[] stride;
    /** 表示扩张卷积在height和weight上的扩张率，是一个长度为2的int数组[dilationHeight，dilationWidth]，
     *  值必须大于或等于1，并且不能超过x的height和width。
     */
    long[] dilation;
    /** 填充类型，详情请参考：{@link PadMode} */
    enum PadMode padMode;
    /** 输入x周围的填充，是一个长度为4的int数组[top，bottom，left，right]。 */
    long[] padList;
    /**
     * group，将输入x按inChannel分组。
     * group等于1，这是常规卷积；
     * group大于1且小于或等于inChannel，这是分组卷积。
     */
    long group;
    /** 输入通道数。 */
    long inChannel;
    /** 输出通道数。 */
    long outChannel;
    /** 激活函数类型，详情请参考：{@link ActivationType}。 */
    enum ActivationType activationType;
    /**
     * 一个长度为的2整数列表，指定沿输出张量的高度和宽度的填充量。
     */
    long[] outputPaddings;
};

/**
 * @brief 将两个tensor执行除法运算。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_DIV_FUSION。
 *
 * 输入：
 *
 * * x1，int或float类型的张量。
 * * x2，int或float类型的张量。
 *
 * 输出：
 *
 * * output， 输出两输入相除后的结果。
 *
 * @since 3.2
 * @version 1.0
 */
struct DivFusion
{
    /** 激活函数类型，详情请参考：{@link Activation}。 */
    enum ActivationType activationType;
};

/**
 * @brief 元素级别操作的算子。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_ELTWISE。
 *
 * 输入：
 *
 * * x1，第一个输入张量。
 * * x2，第二个输入张量。
 *
 * 输出：
 *
 * * output，与x1有相同的数据类型和形状。
 *
 * @since 3.2
 * @version 1.0
 */
struct Eltwise
{
    /** 元素级别操作的类型，详情请参考：{@link EltwiseMode}。 */
    enum EltwiseMode mode;
};

/**
 * @brief 在给定轴上为tensor添加一个额外的维度。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_EXPAND_DIMS。
 *
 * 输入：
 *
 * * x，n维tensor
 * * axis，需要添加的维度的index，int32_t类型，值必须在[-dim-1，dim]，且只允许常量值。
 *
 * 输出：
 *
 * * output，给定轴上添加了额外额度的算子。
 *
 * @since 3.2
 * @version 1.0
 */
struct ExpandDims
{
};

/**
 * @brief 根据指定的维度，创建由一个标量填充的tensor。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_FILL。
 *
 * 输入：
 *
 * * value，填充的标量。
 * * shape，指定创建张量的维度。
 * 输出：
 *
 * * output，填充之后的tensor。
 *
 * @since 3.2
 * @version 1.0
 */
struct Fill
{
};

/**
 * @brief 对输入数据做全连接。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_FULL_CONNECTION。
 *
 * 参数限制：useAxis为true的时候axis必须设置，useAxis为false的时候axis为0。
 *
 * 输入：
 *
 * * x，n维tensor
 * * weight，全连接的权重张量。
 * * bias，全连接的偏置，在量化场景下不需要量化参数，其量化版本要求输入 int32 类型数据，实际量化参数由 x 和 weight 共同决定。
 * *
 * 输出：
 *
 * * output，输出运算后的张量。
 *
 * @since 3.2
 * @version 1.0
 */
struct FullConnection
{
    /** 是否使用bias。 */
    boolean hasBias;
    /** 是否使用轴。 */
    boolean useAxis;
    /** 指定输入张量做全连接的轴，从指定轴axis开始，将axis和axis之后的轴展开成1维张量之后再做全连接。 */
    long axis;
    /** 激活函数类型，详情请参考：{@link ActivationType}。 */
    enum ActivationType activationType;
};

/**
 * @brief 对一个tensor进行批标准化的运算。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_FUSED_BATCH_NORM。
 *
 * 输入：
 *
 * * x，n维tensor，要求形状为[N，...，C]，即第n维是通道数（channel）。
 * * scale，缩放因子的1D张量，用于缩放归一化的第一个张量。
 * * offset，用于偏移的1D张量，以移动到归一化的第一个张量。
 * * mean，总体均值的一维张量，仅用于推理；对于训练，必须为空。
 * * variance，用于总体方差的一维张量。仅用于推理；对于训练，必须为空。
 *
 * 输出：
 *
 * * output，输出运算后的张量。
 *
 * @since 3.2
 * @version 1.0
 */
struct FusedBatchNorm
{
    /** 趋于零的极小值，用于保证除数不为0。 */
    float epsilon;
};

/**
 * @brief 根据指定的索引和轴返回输入tensor的切片。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_GATHER。
 *
 * 输入：
 *
 * * x，n维tensor。
 * * inputIndices，指定输入x在axis上的索引，是一个int类型的数组，值必须在[0,x.shape[axis])范围内。
 * * axis，指定x被切片的轴，int32_t类型的数组，数组长度为1。
 *
 * 输出：
 *
 * * output，输出切片后的tensor。
 *
 * @since 3.2
 * @version 1.0
 */
struct Gather
{
};

/**
 * @brief 对一个tensor从某一axis开始做层归一化。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_LAYER_NORM_FUSION。
 *
 * 输入：
 *
 * * x，n维tensor。
 * * gamma，一个m维tensor，gamma维度应该与input做归一化部分的shape一致。
 * * beta，一个m维tensor，shape与gamma一样。
 *
 * 输出：
 *
 * * output，n维输出tensor，数据类型和shape和input一致。
 *
 * @since 3.2
 * @version 1.0
 */
struct LayerNormFusion
{
    /**  指定x需进行层归一化的起始维度。 */
    long beginNormAxis;
    /** 为数值稳定性定义一个加到分母上的值。 */
    float epsilon;
    /** 是否为元素级别的操作。 */
    boolean elementwiseAffine;
    /** 指定输入参数gamma，beta需进行层归一化的开始维度，其值必须在[-n, n)范围内。 */
    long beginParamsAxis;
};

/**
 * @brief 对输入x1和x2，计算每对元素的x1<=x2的结果。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_LESS_EQUAL。
 *
 * 输入：
 *
 * * x1，可以是实数、布尔值或数据类型是实数或者布尔类型的tensor。
 * * x2，如果x1是tensor，x2可以是实数、布尔值，否则只能是tensor，其数据类型是实数或DATA_TYPE_BOOL。
 *
 * 输出：
 *
 * * output，数据类型为DATA_TYPE_BOOL的tensor；使用量化模型时，output的量化参数不可省略，但量化参数的数值不会对输入结果产生影响。
 *
 * @since 3.2
 * @version 1.0
 */
struct LessEqual
{
};

/**
 * @brief 对输入x1和x2，计算x1和x2的内积。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_MATMUL_FUSION。
 *
 * 输入：
 *
 * * x1，n维输入tensor，实数或DATA_TYPE_BOOL类型的tensor。
 * * x2，n维输入tensor，实数或DATA_TYPE_BOOL类型的tensor。
 *
 * 输出：
 *
 * * output，计算得到内积，当type!=DATA_TYPE_UNKNOWN时，output数据类型由type决定；当type==DATA_TYPE_UNKNOWN时，
 *      output的数据类型取决于x1和x2进行计算时转化的数据类型。
 *
 * @since 3.2
 * @version 1.0
 */
struct MatMulFusion
{
    /** 是否对x1矩阵进行转置。 */
    boolean transposeA;
    /** 是否对x2矩阵进行转置。 */
    boolean transposeB;
    /** 激活函数，详情请参考：{@link ActivationType}。 */
    enum ActivationType activationType;
};

/**
 * @brief 对输入x1和x2，计算x1和x2对应元素最大值，x1和x2的输入遵守隐式类型转换规则，使数据类型一致。
 * 
 * 输入必须是两个张量或一个张量和一个标量。当输入是两个张量时，它们的数据类型不能同时为DATA_TYPE_BOOL。它们的形状支持
 * broadcast成相同的大小。当输入是一个张量和一个标量时，标量只能是一个常数。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_MAXIMUM。
 *
 * 输入：
 *
 * * x1，n维输入tensor，实数或DATA_TYPE_BOOL类型。
 * * x2，n维输入tensor，实数或DATA_TYPE_BOOL类型。
 *
 * 输出：
 *
 * * output，x1和x2两个tensor对应元素的最大值。
 *
 * @since 3.2
 * @version 1.0
 */
struct Maximum
{
};

/**
 * @brief  对输入x，计算 2D 最大值池化。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_MAX_POOL_FUSION。
 *
 * 参数限制：当padMode==PAD_MODE_PAD时，padList的数值必须大于等于0。其他情况下padding的数值必须为0。
 *
 * 输入：
 *
 * * x，n维tensor
 *
 * 输出：
 *
 * * output，x1和x2两个tensor对应元素的最大值。
 *
 * @since 3.2
 * @version 1.0
 */
struct MaxPoolFusion
{
    /** 取最大值的kernel大小，是一个长度为2的int数组[kernel_height，kernel_weight]。 */
    long[] kernelSize;
    /** 池化核步长，kernel移动的距离，是一个长度为2的int数组。 */
    long[] strides;
    /** 填充数组。 */
    long[] pad;
    /** 填充类型，详情请参考：{@link PadMode}。 */
    enum PadMode padMode;
    /** 运算时数据的排列，详情请参考：{@link Format}。 */
    enum Format format;
    /** 是否是全局池化。 */
    boolean global;
    /** 激活函数，详情请参考：{@link ActivationType}。 */
    enum ActivationType activationType;
};

/**
 * @brief 对输入x1和x2，将x1和x2相同的位置的元素相乘得到output。
 *  
 * 如果x1和x2类型shape不同，要求x1和x2可以通过broadcast扩充成相同的shape进行相乘。
 * 该算子对应的{@link NodeType}为NODE_TYPE_MUL_FUSION。
 *
 * 输入：
 *
 * * x1，int或float类型的张量。
 * * x2，int或float类型的张量。
 *
 * 输出：
 *
 * * x1和x2每个元素的乘积。
 *
 * @since 3.2
 * @version 1.0
 */
struct MulFusion
{
    /** 激活函数，详情请参考：{@link ActivationType}。 */
    enum ActivationType activationType;
};

/**
 * @brief  根据indices指定的位置，生成一个由one-hot向量构成的tensor。
 *
 * 每个onehot向量中的有效值由on_value决定，其他位置由off_value决定。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_ONE_HOT。
 *
 * 输入：
 *
 * * indices，n维tensor。indices中每个元素决定每个one-hot向量，on_value的位置。
 * * depth，一个整型标量，决定one-hot向量的深度。要求depth>0。
 * * on_value，一个标量，指定one-hot向量中的有效值。
 * * off_value，一个标量，指定one-hot向量中除有效位以外，其他位置的值。
 *
 * 输出：
 *
 * * output，如果indices时n维tensor，则output是(n+1)维tensor。output的形状由indices和axis共同决定。
 *
 * @since 3.2
 * @version 1.0
 */
struct OneHot
{
    /**
     * 一个整型标量，指定插入one-hot的维度。
     * indices的形状是[N，C]，depth的值是D，当axis=0时，output形状为[D，N，C]，
     * indices的形状是[N，C]，depth的值是D，当axis=-1时，output形状为[N，C，D]，
     * indices的形状是[N，C]，depth的值是D，当axis=1时，output形状为[N，D，C]。
     *
     */
    long axis;
};

/**
 * @brief  在x指定维度的数据前后，添加指定数值进行增广。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_PAD_FUSION。
 *
 * 参数限制：当paddingMode==PADDING_MODE_CONSTANT时，需要设置constantValue，默认constantValue为0。
 *
 * 输入：
 *
 * * x，n维tensor
 * * paddings，一个2维tensor，指定每一维度增补的长度，shape为[n，2]。paddings[i][0]表示第i维上，需要在输入张量前增补的数量；
 *   paddings[i][1]表示第i维上，需要在输入张量x后增补的数量。
 *
 * 输出：
 *
 * * output，一个n维tensor，维数和数据类型和x保持一致。shape由x和paddings共同决定
 *      output.shape[i] = input.shape[i] + paddings[i][0]+paddings[i][1]。
 *
 * @since 3.2
 * @version 1.0
 */
struct PadFusion
{
    /**
     * 一个2维tensor，指定每一维度增补的长度，shape为[n，2]。paddings[i][0]表示第i维上，需要在x1前增补的数量；
     * paddings[i][1]表示第i维上，需要在x1后增补的数量。
     * 该参数和输入的paddings意义相同。
     */
    long[][] paddings;
    /** 
     * 填充类型。
     * 详情请参考：{@link PaddingMode}。
     */
    enum PaddingMode paddingMode;
    /** 
     * 一个常数，数据类型和x一致，指定Pad操作增广的数值。
     * 仅paddingMode==PADDING_MODE_CONSTANT时生效，默认值为0。
     */
    float constantValue;
};

/**
 * @brief 求x的y次幂，输入必须是两个tensor或一个tensor和一个标量。
 * 
 * 当输入是两个tensor时，它们的数据类型不能同时为DATA_TYPE_BOOL，且要求两个tensor的shape相同。当输入是一个tensor和一个标量时，标量只能是一个常数。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_POW_FUSION。
 *
 * 参数说明：x的每个元素会做如下运算：x' = scale*x+shift，然后对于x'再求y次幂。
 *
 * 输入：
 *
 * * x，实数、bool值或tensor，tensor的数据类型为实数或DATA_TYPE_BOOL。
 * * y，实数、bool值或tensor，tensor的数据类型为实数或DATA_TYPE_BOOL。
 *
 * 输出：
 *
 * * output，形状由x和y broadcast后的形状决定。
 *
 * @since 3.2
 * @version 1.0
 */
struct PowFusion
{
    /** 对x的数值进行缩放。 */
    float scale;
    /** 对x的数值缩放后的值进行增减。 */
    float shift;
};

/**
 * @brief 计算x和weight的PReLU激活值。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_PRELU_FUSION。
 *
 * 输入：
 *
 * *  x，一个n维tensor，如果n>=2，则要求x1的排布为[BatchSize，…，Channels]，第二个维度为通道数。
 * *  weight，一个1维tensor。weight的长度只能是1或者等于通道数。当weight长度为1，则x所有通道共享一个权重值。
 *       若weight长度等于通道数，每个通道独享一个权重，若x维数n<2，weight长度只能为1。
 *
 * 输出：
 *
 * * output，x的PReLU激活值。形状和数据类型和x保持一致。
 *
 * @since 3.2
 * @version 1.0
 */
struct PReLUFusion
{
    /**
     * 是否开启权重共享，可以用于校验参数合法性。
     * 若weight的是1则channelShared一定为true，否则为false。
     */
    boolean channelShared;
};

/**
 * @brief 数据类型转换。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_QUANT_DTYPE_CAST。
 *
 * 输入：
 *
 * * x，n维tensor。
 *
 * 输出：
 *
 * * output，类型转换之后的tensor。
 *
 * @since 3.2
 * @version 1.0
 */
struct QuantDTypeCast
{
    /** 定义输入的数据类型。 */
    long srcT;
    /** 定义输出的数据类型。 */
    long dstT;
};

/**
 * @brief 减小x张量的维度。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_REDUCE_FUSION。
 *
 * 参数说明：mode若为REDUCE_ALL,REDUCE_PROD和REDUCE_MEAN，则reduce_to_end为true时，Reduce之后的输出乘以coeff为最终输出。
 *
 * 输入：
 *
 * * x，n维tensor，n<8。
 * * axis，1维tensor，指定reduce的维度，axis中每个元素的取值范围为[-n，n)。
 *
 * 输出：
 *
 * *  output，执行Reduce之后的m维的tensor，其数据类型和x相同。当keepDims为false时，m<n；当keepDims为true时，m==n。
 *
 * @since 3.2
 * @version 1.0
 */
struct ReduceFusion
{
    /** 维度是否保持不变。 */
    boolean keepDims;
    /** 减小张量维度的算法，详情请参考：{@link ReduceMode}。 */
    enum ReduceMode mode;
    /**
     * 如果为true，则从axis取第一个元素并设置为i，
     * 然后axis会被修改为[i,i+1,...,n-1,n]，
     * 例如reduceToEnd=True，axis=[2,4]，x的维度为7，则axis会被修改为[2,3,4,5,6]
     */
    boolean reduceToEnd;
    /** 系数 */
    float coeff;
};

/**
 * @brief 根据inputShape调整input的形状。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_RESHAPE。
 *
 * 输入：
 *
 * * x，一个n维输入tensor。
 * * InputShape，一个1维tensor，表示输出tensor的shape，需要是一个常量tensor。
 *
 * 输出：
 *
 * * output，数据类型和input一致，shape由inputShape决定。
 *
 * @since 3.2
 * @version 1.0
 */
struct Reshape
{
};

/**
 * @brief 按给定的参数对输入的张量进行变形。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_RESIZE。
 *
 * 参数指导：该算子的参数组合可以实现常用的Resize函数。
 * 例如，实现精确对齐图像的4个角的双线性插值则设置：
 * method = RESIZE_METHOD_LINEAR
 * coordinateTransformMode = COORDINATE_TRANSFORM_MODE_ALIGN_CORNERS
 *
 * 输入：
 *
 * * x，一个4维tensor，tensor排布必须是[batchSize，height，width，channels](NHWC)。
 *
 * 输出：
 *
 * * output，n维输出tensor，它的的shape和数据类型与x相同。
 *
 * @since 3.2
 * @version 1.0
 */
struct Resize
{
    /** 调整尺寸的方法，详情请参考：{@link ResizeMethod}。 */
    enum ResizeMethod method;
    /** resize之后4维tensor的height值。 */
    long newHeight;
    /** resize之后4维tensor的width值。 */
    long newWidth;
    /** 一个布尔值，指示resize操作是否保持x张量的height/width比例。 */
    boolean preserveAspectRatio;
    /**
     * 坐标变换方法，详情请参考：{@link CoordinateTransformMode}。
     */
    enum CoordinateTransformMode coordinateTransformMode;
    /** 立方系数，当method为RESIZE_METHOD_CUBIC时使用。 */
    float cubicCoeff;
    /** 当excludeOutside==1时，超出x的边界的采样权重被置为0，其余权重重新归一化处理。 */
    long excludeOutside;
    /** 外插值，当仅用于裁剪x的时候使用，超出边界的采样权重被置为extrapolationValue。 */
    float extrapolationValue;
    /** 最近邻近算法，当method==RESIZE_METHOD_NEAREST时使用，详情请参考：{@link NearestMode}。 */
    enum NearestMode nearestMode;
};

/**
 * @brief 求x的平方根的倒数。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_RSQRT。
 *
 * 输入：
 *
 * *x，n维输入tensor，input中的每个元素不能小于0，n<8。
 *
 * 输出：
 *
 * * output，n维输出tensor，output的shape和数据类型和input相同。
 *
 * @since 3.2
 * @version 1.0
 */
struct Rsqrt
{
};

/**
 * @brief 给定一个tensor，计算其缩放后的值。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_SCALE_FUSION。
 *
 * 输入：
 *
 * * x，n维tensor。
 * * scale，缩放tensor。
 * * bias，偏置tensor。
 *
 * 输出：
 *
 * * output， scale的计算结果，一个n维tensor，类型和x一致，shape由axis决定。
 *
 * @since 3.2
 * @version 1.0
 */
struct ScaleFusion
{
    /** 指定缩放的维度。 */
    long axis;
    /** 激活函数，详情请参考：{@link ActivationType}。 */
    enum ActivationType activationType;
};

/**
 * @brief 输出输入tensor的形状。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_SHAPE。
 *
 * 输入：
 *
 * * x，n维tensor
 *
 * 输出：
 *
 * * output，输出x的维度，一个整型数组。
 *
 * @since 3.2
 * @version 1.0
 */
struct Shape
{
};

/**
 * @brief 在x各维度，在axes维度中，以begin为起点，截取size长度的切片。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_SLICE_FUSION。
 *
 * 输入：
 *
 * * x，n维输入tensor。
 * * begin，一组不小于0的整数，指定axes维度上的起始切分点。
 * * size，一组不小于1的整数，指定axes维度上切片的长度。假设某一维度i，1<=size[i]<=input.shape[i]-begin[i]。
 *
 * 输出：
 *
 * * output，切片得到的n维tensor。
 *
 * @since 3.2
 * @version 1.0
 */
struct SliceFusion
{
    /** 作用的维度。 */
    long[] axes;
};

/**
 * @brief 给定一个tensor，计算其softmax结果。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_SOFTMAX。
 *
 * 输入：
 *
 * * x，n维tensor
 *
 * 输出：
 *
 * * output，softmax的计算结果，一个n维tensor，类型和shape和x一致。
 *
 * @since 3.2
 * @version 1.0
 */
struct Softmax
{
    /** 指定计算softmax的维度。整数取值范围为[-n，n)。 */
    long[] axis;
};

/**
 * @brief 将4维张量在空间维度上进行切分成多个小块，然后在batch维度上拼接这些小块。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_SPACE_TO_BATCH_ND。
 *
 * 输入：
 *
 * * x，n维tensor
 *
 * 输出：
 *
 * * output，一个4维tensor，数据类型和input一致。shape由input，blockShape和paddings共同决定，假设input shape为[n,c,h,w]，则有：
 * \f$ output.shape[0] = n * blockShape[0] * blockShape[1]\f$<br>
 * \f$ output.shape[1] = c \f$<br>
 * \f$ output.shape[2] = (h + paddings[0][0] + paddings[0][1]) / blockShape[0] \f$<br>
 * \f$ output.shape[3] = (w + paddings[1][0] + paddings[1][1]) / blockShape[1] \f$<br>
 * 要求\f$ (h + paddings[0][0] + paddings[0][1])能被\f$ blockShape[0]\f$整除，(w + paddings[1][0] + paddings[1][1]) \f$能被\f$ blockShape[1] \f$整除。
 *
 * @since 3.2
 * @version 1.0
 */
struct SpaceToBatchND
{
    /** 描述空间维度为分割的个数，取值需大于1。 */
    long[] blockShape;
    /** 空间维度的填充大小。 */
    long[][] paddings;
};

/**
 * @brief 算子沿 axis 维度将x拆分成多个张量，张量数量由outputNum指定。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_SPLIT。
 *
 * 输入：
 *
 * * x，n维tensor
 *
 * 输出：
 *
 * *  outputs，一组n维张量，每一个张量类型和维度相同，每个张量的类型和x一致。
 *
 * @since 3.2
 * @version 1.0
 */
struct Split
{
    /** 指定分割数量。 */
    long outputNum;
    /**
     * 指定 输入的张量沿 axis 轴拆分后，每个张量的大小。
     * 如果 sizeSplits 的数据为空，则 sizeSplits 被拆分成大小均等的 张量，此时要求 x.shape[axis] 可以被 outputNum 整除；
     * 如果 sizeSplits 不为空，则要求 sizeSplits 所有元素之和等于 x.shape[axis]。
     */
    long[] sizeSplits;
    /** 指定分割的维度。 */
    long axis;
};

/**
 * @brief 给定一个tensor，计算其平方根。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_SQRT。
 *
 * 输入：
 *
 * * x，n维tensor。
 *
 * 输出：
 *
 * * output，一个n维tensor，类型和shape和x一致。
 *
 * @since 3.2
 * @version 1.0
 */
struct Sqrt
{
};

/**
 * @brief 计算两个输入的差值并返回差值的平方。SquaredDifference算子支持tensor和tensor相减。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_SQUEEZE。
 *
 * 输入：
 *
 * * x，被减数，是一个tensor，tensor的类型可以是实数或DATA_TYPE_BOOL。
 * * y，减数，是一个tensor，tensor的类型可以是实数或DATA_TYPE_BOOL。
 *
 * 输出：
 *
 * * output，output的shape由x1和y共同决定，x1和y的shape相同时，
 *      output的shape和x1、y相同；shape不同时，需要将x1或y做broadcast操作后，相减得到output。
 *      output的精度由两个输入中更高精度的决定。
 *
 * @since 3.2
 * @version 1.0
 */
struct SquaredDifference
{
};

/**
 * @brief 去除axis中，长度为1的维度。支持int8量化输入。
 * 
 * 假设输入的x的shape为[2，1，1，2，2]，axis为[0,1]，则输出的output的shape为[2，1，2，2]，意思是第0维到第1维之间，长度为1的维度被去除。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_SQUARED_DIFFERENCE。
 *
 * 输入：
 *
 * * x，n维tensor
 *
 * 输出：
 *
 * * output，去除axis中长度为1的维度之后得到的tensor。
 *
 * @since 3.2
 * @version 1.0
 */
struct Squeeze
{
    /**  指定删除的维度。axis可以是一个整数或数组，整数的取值范围为[-n，n)。 */
    long[] axis;
};

/**
 * @brief 将一组tensor沿axis维度进行堆叠，堆叠前每个tensor的维数为n，则堆叠后output维数为n+1。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_STACK。
 *
 * 输入：
 *
 * * 多个输入n维tensor，每个tensor要求shape相同且类型相同。
 *
 * 输出：
 *
 * * output，将输入的张量沿axis维度堆叠的输出，n+1维tensor，数据类型与精度和输入相同。
 *
 * @since 3.2
 * @version 1.0
 */
struct Stack
{
    /** 一个整数，指定tensor堆叠的维度。axis可以是负数，axis取值范围为[-(n+1)，(n+1))。 */
    long axis;
};

/**
 * @brief 根据步长和索引对输入张量进行切片提取。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_STRIDED_SLICE。
 *
 * 输入：
 *
 * * x，n维tensor。
 * * begin，1维tensor，begin的长度等于n，begin[i]指定第i维上截取的起点。
 * * end，1维tensor，end的长度等于n，end[i]指定第i维上截取的终点。
 * * strides，1维张量，strides的长度等于n，strides[i]指定第i维上截取的步长，允许存在负值。
 *
 * 输入的张量有如下情况：begin, end 和 strides 的shape必须相同。 begin,end 是从0开始进行索引。 strides 的元素必须非零。
 *
 * 输出：
 *
 * * output，堆叠运算后的tensor，数据类型与x相同。输出维度rank(x[0])+1维。
 *
 * @since 3.2
 * @version 1.0
 */
struct StridedSlice
{
    /**
     * 表示切片的起始索引。
     * beginMask使用二进制编码方式对输入x的不同维度进行标志，beginMask的第i位设置为1则begin[i]参数对应的第i维度设置无效，表示该维度的起始索引从0开始。默认值为0。
     */
    long beginMask;
    /**
     * 表示切片的结束索引。功能类似begin_mask。
     * endMask使用二进制编码方式对输入x的不同维度进行标志，第i位设置为1则end参数对应的该维度设置无效，表示该维度切分的结束索引到列表最后，即切分到尽可能大的维度。默认值为0。 
     */
    long endMask;
    /**
     * 一个整数，用于解除begin和end的限制。
     * 不为0的维度不需要进行切片操作。
     * 将ellipsisMask转成二进制表示，如果ellipsisMask的第i位为1，则对于第i维，从第一个元素开始，以strides[i]为步长，截取元素直到tensor边界。
     */
    long ellipsisMask;
    /**
     * 用于新增维度。
     * newAxisMask使用二进制编码方式对输入x的不同维度进行标志，如果第i位出现1，则begin、end、stride对所有维度参数无效，并在第i位上增加一个大小为1的维度。
     */
    long newAxisMask;
    /**
     * 用于压缩指定维度。
     * 将shrinkAxisMask转成二进制表示，如果shrinkAxisMask的第i位位1，则舍去第i维所有元素，第i维长度压缩至1。
     */
    long shrinkAxisMask;
};

/**
 * @brief 计算两个输入的差值。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_SUB_FUSION。
 *
 * 输入：
 *
 * * x，被减数，是int或float类型的张量。
 * * y，减数，是int或float类型的张量。
 *
 * 输出：
 *
 * * output，两个input相减的差。output的shape由x和y共同决定，x和y的shape相同时，output的shape和x、y相同；
 *      shape不同时，需要将x或y做broadcast操作后，相减得到output。output的精度为x和y中精度更高的决定。
 *
 * @since 3.2
 * @version 1.0
 */
struct SubFusion
{
    /** 激活函数类型，详情请参考：{@link ActivationType}。 */
    enum ActivationType activationType;
};

/**
 * @brief 以multiples指定的次数拷贝输入张量。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_TILE_FUSION。
 *
 * 输入：
 *
 * * x，n维tensor。
 * * multiples， 1维tensor，指定各个维度拷贝的次数。其长度m不小于x的维数n。
 *
 * 输出：
 *
 * * Tensor，m维tensor，OperandType与input相同。如果input和multiples长度相同，
 *      则output和input维数一致，都是n维tensor；如果multiples长度大于n，则用1填充input的维度，
 *      再在各个维度上拷贝相应的次数，得到m维tensor。
 *
 * @since 3.2
 * @version 1.0
 */
struct TileFusion
{
    /** 它和multiples功能相同，1维tensor，指定各个维度拷贝的次数。其长度m不小于x的维数n。*/
    long[] dims;
};

/**
 * @brief 查找沿axis轴的前K个最大值和对应索引。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_TOPK_FUSION。
 *
 * 输入：
 *
 * * x，n维tensor。
 *
 * 输出：
 *
 * * output0，axis维度的前K个最大值。
 * * output1，axis维度的前K个最大值的索引。
 *
 * @since 3.2
 * @version 1.0
 */
struct TopKFusion
{
    /** 如果为True，按照大到小排序，如果为False，按照小到大排序。 */
    boolean sorted;
    /** 作用的轴 */
    long axis;
};

/**
 * @brief  根据perm对x进行数据重排。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_TRANSPOSE。
 *
 * 输入：
 *
 * * x，n维tensor，待重排的tensor。
 * * perm，1维tensor，其长度和x的维数一致。
 *
 * 输出：
 *
 * * output，n维tensor，output的数据类型，量化等参数与x相同，shape由x的shape和perm共同决定。
 *
 * @since 3.2
 * @version 1.0
 */
struct Transpose
{
};

/**
 * @brief 根据输入axis的值。增加一个维度。
 *
 * 该算子对应的{@link NodeType}为NODE_TYPE_UNSQUEEZE。
 *
 * 输入：
 *
 * * x，n维tensor。
 *
 * 输出：
 *
 * * output，输出tensor。
 *
 * @since 3.2
 * @version 1.0
 */
struct Unsqueeze
{
    /** axis，指定增加的维度。axis可以是一个整数或一组整数，整数的取值范围为[-n，n)。 */
    long[] axis;
};

/** @} */