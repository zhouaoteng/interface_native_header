/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OH_VALUES_BUCKET_H
#define OH_VALUES_BUCKET_H

/**
 * @addtogroup RDB
 * @{
 *
 * @brief The relational database (RDB) store manages data based on relational models.
 * A complete set of mechanisms for managing local databases is provided based on the underlying SQLite.
 * To satisfy different needs in complicated scenarios, the RDB module provides a series of methods for performing
 * operations such as adding, deleting, modifying, and querying data, and supports direct execution of SQL statements.
 *
 * @syscap SystemCapability.DistributedDataManager.RelationalStore.Core
 * @since 10
 */

/**
 * @file oh_values_bucket.h
 *
 * @brief Defines the types of the key and value in a key-value (KV) pair.
 * @library native_rdb_ndk_header.so
 * @since 10
 */

#include <cstdint>
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines the types of the key and value in a KV pair.
 *
 * @since 10
 */
typedef struct OH_VBucket {
    /** Unique identifier of the OH_VBucket struct. */
    int64_t id;

    /** Number of the KV pairs in the struct. */
    uint16_t capability;

    /**
     * @brief Puts a char value into the {@link OH_VBucket} instance in the given column.
     *
     * @param bucket Indicates the pointer to the {@link OH_VBucket} instance.
     * @param field Indicates the pointer to the column name.
     * @param value Indicates the pointer to the value to put.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_VBucket.
     * @since 10
     */
    int (*putText)(OH_VBucket *bucket, const char *field, const char *value);

    /**
     * @brief Puts an int64_t value into the {@link OH_VBucket} instance in the given column.
     *
     * @param bucket Indicates the pointer to the {@link OH_VBucket} instance.
     * @param field Indicates the pointer to the column name.
     * @param value Indicates the value to put.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_VBucket.
     * @since 10
     */
    int (*putInt64)(OH_VBucket *bucket, const char *field, int64_t value);

    /**
     * @brief Puts a double value into the {@link OH_VBucket} instance in the given column.
     *
     * @param bucket Indicates the pointer to the {@link OH_VBucket} instance.
     * @param field Indicates the pointer to the column name.
     * @param value Indicates the value to put.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_VBucket.
     * @since 10
     */
    int (*putReal)(OH_VBucket *bucket, const char *field, double value);

    /**
     * @brief Puts a const uint8_t value into the {@link OH_VBucket} object in the given column.
     *
     * @param bucket Indicates the pointer to the {@link OH_VBucket} instance.
     * @param field Indicates the pointer to the column name.
     * @param value Indicates the pointer to the value to put.
     * @param size Indicates the length of the value.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_VBucket.
     * @since 10
     */
    int (*putBlob)(OH_VBucket *bucket, const char *field, const uint8_t *value, uint32_t size);

    /**
     * @brief Puts null into the {@link OH_VBucket} instance in the given column.
     *
     * @param bucket Indicates the pointer to the {@link OH_VBucket} instance.
     * @param field Indicates the pointer to the column name.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_VBucket.
     * @since 10
     */
    int (*putNull)(OH_VBucket *bucket, const char *field);

    /**
     * @brief Clears an {@link OH_VBucket} instance.
     *
     * @param bucket Indicates the pointer to the {@link OH_VBucket} instance.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_VBucket.
     * @since 10
     */
    int (*clear)(OH_VBucket *bucket);

    /**
     * @brief Destroys a {@link OH_VBucket} instance and reclaims the memory occupied.
     *
     * @param bucket Indicates the pointer to the {@link OH_VBucket} instance.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_VBucket.
     * @since 10
     */
    int (*destroy)(OH_VBucket *bucket);
} OH_VBucket;

#ifdef __cplusplus
};
#endif

#endif // OH_VALUES_BUCKET_H
