/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Core
 * @{
 *
 * @brief Provides the basic backbone capabilities for the media playback framework,
 * including functions related to the memory, error code, and format carrier.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 *
 * @since 9
 * @version 1.0
 */

/**
 * @file native_avformat.h
 *
 * @brief Declares the format-related functions and enums.
 *
 * @since 9
 * @version 1.0
 */

#ifndef NATIVE_AVFORMAT_H
#define NATIVE_AVFORMAT_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct OH_AVFormat OH_AVFormat;

/**
 * @brief Enumerates the audio and video pixel formats.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @since 9
 * @version 1.0
 */
typedef enum OH_AVPixelFormat {
    /**
     * YUV 420 Planar.
     */
    AV_PIXEL_FORMAT_YUVI420 = 1,
    /**
     * NV12. YUV 420 Semi-planar.
     */
    AV_PIXEL_FORMAT_NV12 = 2,
    /**
     * NV21. YVU 420 Semi-planar.
     */
    AV_PIXEL_FORMAT_NV21 = 3,
    /**
     * Surface.
     */
    AV_PIXEL_FORMAT_SURFACE_FORMAT = 4,
    /**
     * RGBA8888.
     */
    AV_PIXEL_FORMAT_RGBA = 5,
} OH_AVPixelFormat;

/**
 * @brief Creates an <b>OH_AVFormat</b> instance for reading and writing data.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @return Returns the handle to an <b>OH_AVFormat</b> instance.
 * @since 9
 * @version 1.0
 */
struct OH_AVFormat *OH_AVFormat_Create(void);

/**
 * @brief Destroys an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @return void
 * @since 9
 * @version 1.0
 */
void OH_AVFormat_Destroy(struct OH_AVFormat *format);

/**
 * @brief Copies the resources from an <b>OH_AVFormat</b> instance to another.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param to Indicates the handle to the <b>OH_AVFormat</b> instance to which the data will be copied.
 * @param from Indicates the handle to the <b>OH_AVFormat</b> instance from which the data will be copied.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_Copy(struct OH_AVFormat *to, struct OH_AVFormat *from);

/**
 * @brief Writes data of the int type to an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @param key Indicates the pointer to the key of the data to write.
 * @param value Indicates the value of the data to write.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_SetIntValue(struct OH_AVFormat *format, const char *key, int32_t value);

/**
 * @brief Writes data of the long type to an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @param key Indicates the pointer to the key of the data to write.
 * @param value Indicates the value of the data to write.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_SetLongValue(struct OH_AVFormat *format, const char *key, int64_t value);

/**
 * @brief Writes data of the float type to an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @param key Indicates the pointer to the key of the data to write.
 * @param value Indicates the value of the data to write.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_SetFloatValue(struct OH_AVFormat *format, const char *key, float value);

/**
 * @brief Writes data of the double type to an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @param key Indicates the pointer to the key of the data to write.
 * @param value Indicates the value of the data to write.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_SetDoubleValue(struct OH_AVFormat *format, const char *key, double value);

/**
 * @brief Writes data of the string type to an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @param key Indicates the pointer to the key of the data to write.
 * @param value Indicates the pointer to the value of the data to write.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_SetStringValue(struct OH_AVFormat *format, const char *key, const char *value);

/**
 * @brief Writes data with a specified size to an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @param key Indicates the pointer to the key of the data to write.
 * @param addr Indicates the pointer to the address where the data is written.
 * @param size Indicates the size of the data written.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_SetBuffer(struct OH_AVFormat *format, const char *key, const uint8_t *addr, size_t size);

/**
 * @brief Reads data of the int type from an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @param key Indicates the pointer to the key of the data to read.
 * @param out Indicates the pointer to the data read.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_GetIntValue(struct OH_AVFormat *format, const char *key, int32_t *out);

/**
 * @brief Reads data of the long type from an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @param key Indicates the pointer to the key of the data to read.
 * @param out Indicates the pointer to the data read.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_GetLongValue(struct OH_AVFormat *format, const char *key, int64_t *out);

/**
 * @brief Reads data of the float type from an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @param key Indicates the pointer to the key of the data to read.
 * @param out Indicates the pointer to the data read.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_GetFloatValue(struct OH_AVFormat *format, const char *key, float *out);

/**
 * @brief Reads data of the double type from an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @param key Indicates the pointer to the key of the data to read.
 * @param out Indicates the pointer to the data read.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_GetDoubleValue(struct OH_AVFormat *format, const char *key, double *out);

/**
 * @brief Reads data of the double type from an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @param key Indicates the pointer to the key of the data to read.
 * @param out Indicates the double pointer to the data read. The data read is updated when <b>GetString</b>
 * is called and destroyed when the <b>OH_AVFormat</b> instance is destroyed. 
 * To hold the data for an extended period of time, copy it to the memory.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_GetStringValue(struct OH_AVFormat *format, const char *key, const char **out);

/**
 * @brief Reads data with a specified size from an <b>OH_AVFormat</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @param key Indicates the pointer to the key of the data to read.
 * @param addr Indicates the double pointer to the address where the data read is stored. 
 * The data read is destroyed when the <b>OH_AVFormat</b> instance is destroyed.
 * To hold the data for an extended period of time, copy it to the memory.
 * @param size Indicates the pointer to the size of the data read.
 * @return Returns <b>TRUE</b> if the operation is successful.
 * @return Returns <b>FALSE</b> if the operation fails.
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_GetBuffer(struct OH_AVFormat *format, const char *key, uint8_t **addr, size_t *size);

/**
 * @brief Dumps the information contained in an<b>OH_AVFormat</b> instance as a string.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @return Returns the pointer to a collect of strings, each of which consists of a key and value.
 * @since 9
 * @version 1.0
 */
const char *OH_AVFormat_DumpInfo(struct OH_AVFormat *format);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVFORMAT_H
