/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Audio
 * @{
 *
 * @brief Defines audio-related APIs, including data types and functions for loading drivers, accessing a driver adapter, and rendering and capturing audios.
 *
 *
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IAudioAdapter.idl
 *
 * @brief Declares APIs for operations related to the audio adapter.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the audio APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.audio.v1_0;

import ohos.hdi.audio.v1_0.AudioTypes;
import ohos.hdi.audio.v1_0.IAudioRender;
import ohos.hdi.audio.v1_0.IAudioCapture;

/**
 * @brief Provides audio adapter capabilities, including initializing ports, creating rendering and capturing tasks, and obtaining the port capabilities.
 *
 *
 *
 * @see AudioRender
 * @see AudioCapture
 *
 * @since 3.2
 * @version 1.0
 */
interface IAudioAdapter {
    /**
     * @brief Initializes all port drivers of an audio adapter.
     *
     * You must call this function before calling other driver functions to check whether the initialization is complete.
     * If the initialization is not complete, wait for a while (for example, 100 ms) and perform the check again until the initialization is complete.
     *
     * @return Returns <b>0</b> if the initialization is successful; returns other values if the initialization fails.
     *
     * @since 3.2
     * @version 1.0
     */
    InitAllPorts();

    /**
     * @brief Creates a <b>Render</b> object.
     *
     * @param desc Indicates the audio device descriptor. For details, see {@link AudioDeviceDescriptor}.
     * @param attrs Indicates the audio sampling attributes. For details, see {@link AudioSampleAttributes}.
     * @param render Indicates the <b>Render</b> object created. For details, see {@link IAudioRender}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
      *
     * @see GetPortCapability
     * @see DestroyRender
     *
     * @since 3.2
     * @version 1.0
     */
    CreateRender([in] struct AudioDeviceDescriptor desc, [in] struct AudioSampleAttributes attrs, [out] IAudioRender render);

    /**
     * @brief Destroys this <b>Render</b> object.
     *
     * @attention Do not destroy the object during audio rendering.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
      *
     * @see CreateRender
     *
     * @since 3.2
     * @version 1.0
     */
    DestroyRender();

    /**
     * @brief Creates a <b>Capture</b> object.
     *
     * @param desc Indicates the audio device descriptor. For details, see {@link AudioDeviceDescriptor}.
     * @param attrs Indicates the audio sampling attributes. For details, see {@link AudioSampleAttributes}.
     * @param capture Indicates the <b>Capture</b> object created. For details, see {@link IAudioCapture}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see CreateCapture
     *
     * @since 3.2
     * @version 1.0
     */
    CreateCapture([in] struct AudioDeviceDescriptor desc, [in] struct AudioSampleAttributes attrs, [out] IAudioCapture capture);

    /**
     * @brief Destroys this <b>Capture</b> object.
     *
     * @attention Do not destroy the object during audio capturing.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see CreateCapture
     *
     * @since 3.2
     * @version 1.0
     */
    DestroyCapture();

    /**
     * @brief Obtains the capabilities of the port driver.
     *
     * @param port Indicates the target port. For details, see {@link AudioPort}.
     * @param capability Indicates the capabilities obtained. For details, see {@link AudioPortCapability}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    GetPortCapability([in] struct AudioPort port, [out] struct AudioPortCapability capability);

    /**
     * @brief Sets the passthrough data transmission mode of the port driver.
     *
     * @param port Indicates the target port. For details, see {@link AudioPort}.
     * @param mode Indicates the transmission mode to set. For details, see {@link AudioPortPassthroughMode}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see GetPassthroughMode
     *
     * @since 3.2
     * @version 1.0
     */
    SetPassthroughMode([in] struct AudioPort port, [in] enum AudioPortPassthroughMode mode);

    /**
     * @brief Obtains the passthrough data transmission mode of the port driver.
     *
     * @param port Indicates the target port. For details, see {@link AudioPort}.
     * @param mode Indicates the transmission mode obtained. For details, see {@link AudioPortPassthroughMode}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see SetPassthroughMode
     *
     * @since 3.2
     * @version 1.0
     */
    GetPassthroughMode([in] struct AudioPort port, [out] enum AudioPortPassthroughMode mode);

    /**
     * @brief Obtains the device status of the audio adapter.
     *
     * @param status Indicates the device status obtained. For details, see {@link AudioDeviceStatus}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    GetDeviceStatus([out] struct AudioDeviceStatus status);

    /**
     * @brief Updates an audio route.
     *
     * @param route Indicates the route to update. For details, see {@link AudioRoute}.
     * @param routeHandle Indicates the handle to the new route.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    UpdateAudioRoute([in] struct AudioRoute route, [out] int routeHandle);

    /**
     * @brief Releases an audio route.
     *
     * @param routeHandle Indicates the handle to the route to release.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    ReleaseAudioRoute([in] int routeHandle);

    /**
     * @brief Mutes or unmutes the audio.
     *
     * @param mute Indicates whether to mute the audio. The value <b>true</b> means to mute the audio, and <b>false</b> means the opposite.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see SetMicMute
     *
     * @since 3.2
     * @version 1.0
     */
    SetMicMute([in] boolean mute);

    /**
     * @brief Checks whether the audio is muted.
     *
     * @param mute Specifies whether the audio is muted. The value <b>true</b> means that the audio is muted, and <b>false</b> means the opposite.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see GetMicMute
     *
     * @since 3.2
     * @version 1.0
     */
    GetMicMute([out] boolean mute);

    /**
     * @brief Sets the volume of voice calls.
     *
     * @param volume Indicates the volume to set. The value ranges from 0.0 (minimum volume) to 1.0 (maximum volume).
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see SetVoiceVolume
     *
     * @since 3.2
     * @version 1.0
     */
    SetVoiceVolume([in] float volume);

    /**
     * @brief Sets an extended audio parameter based on specified conditions.
     *
     * @param key Indicates the key of the extended parameter. For details, see {@link AudioExtParamKey}.
     * @param condition Indicates the query condition of the extended parameter.
     * @param value Indicates the condition value of the extended parameter.
     *
     * <ul>
     *   <li>1. The value of <b>condition</b> is a string consisting of multiple key-value pairs separated by semicolons (;). The format of a key-value pair is <b>keytype=keyvalue</b>. </li>
     *   <li>2. If <b>key</b> is set to <b>AudioExtParamKey::AUDIO_EXT_PARAM_KEY_VOLUME</b>, the value format of <b>condition</b> must be any of the following:</li>
     *     <ul>
     *       <li>EVENT_TYPE=xxx;VOLUME_GROUP_ID=xxx;AUDIO_VOLUME_TYPE=xxx;</li>
     *       <ul>
     *         <li><b>EVENT_TYPE</b>: volume event type. The value <b>1</b> means to set the volume, and <b>4</b> means to mute the audio. </li>
     *         <li><b>VOLUME_GROUP_ID</b>: volume group related to the extended audio parameter. </li>
     *         <li><b>AUDIO_VOLUME_TYPE</b>: volume type related to the extended audio parameter. </li>
     *        </ul>
     *     </ul>
     * </ul>
     * 
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    SetExtraParams([in] enum AudioExtParamKey key, [in] String condition, [in] String value);

    /**
     * @brief Obtains the value of an extended audio parameter based on specified conditions.
     *
     * @param key Indicates the key of the extended parameter. For details, see {@link AudioExtParamKey}.
     * @param condition Indicates the query condition of the extended parameter.
     * @param value Indicates the condition value of the extended parameter.
     *
     * <ul>
     *   <li>1. The value of <b>condition</b> is a string consisting of multiple key-value pairs separated by semicolons (;). The format of a key-value pair is <b>keytype=keyvalue</b>. </li>
     *   <li>2. If <b>key</b> is set to <b>AudioExtParamKey::AUDIO_EXT_PARAM_KEY_VOLUME</b>, the format of <b>condition</b> must be any of the following:</li>
     *     <ul>
     *       <li>EVENT_TYPE=xxx;VOLUME_GROUP_ID=xxx;AUDIO_VOLUME_TYPE=xxx;</li>
     *       <ul>
     *         <li><b>EVENT_TYPE</b>: volume event type. The value <b>1</b> means to set the volume, and <b>4</b> means to mute the audio. </li>
     *         <li><b>VOLUME_GROUP_ID</b>: volume group related to the extended audio parameter. </li>
     *         <li><b>AUDIO_VOLUME_TYPE</b>: volume type related to the extended audio parameter. </li>
     *        </ul>
     *     </ul>
     * </ul>
     * 
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    GetExtraParams([in] enum AudioExtParamKey key, [in] String condition, [out] String value);

    /**
     * @brief Registers a callback for extended parameters.
     *
     * @param audioCallback Indicates the callback to register. For details, see {@link AudioCallback}.
     * @param cookie Indicates the cookie for data transmission.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */ 
    RegExtraParamObserver([in] AudioCallback audioCallback, [in] byte cookie);
}
/** @} */
